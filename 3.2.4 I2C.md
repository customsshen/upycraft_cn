&nbsp;&nbsp;&nbsp;I2C是设备之间的两线通信协议。在物理层它只需要两个信号线：SCL 和 SDA，分别是时钟和数据线。I2C 对象关联到总线，它可以在创建时初始化，也可以稍后初始化。

### 类

#### class machine.I2C(scl, sda, freq)

	scl：i2c设备时钟引脚对象
    	Pin(0)、Pin(2)、Pin(4)、Pin(5)、Pin(9)、Pin(16~19)、Pin(21~23)、Pin(25~27)	
	sda：i2c设备数据线引脚对象
    	Pin(0)、Pin(2)、Pin(4)、Pin(5)、Pin(9)、Pin(16~19)、Pin(21~23)、Pin(25~27)
	freq：SCL时钟频率
    	0 < freq ≤ 500000(Hz)

#### 定义I2C
示例：

```py
from machine import I2C, Pin

i2c = I2C(scl=Pin(22), sda=Pin(21), freq=10000)
```

### 类函数

#### 1. I2C.init(scl, sda, freq)
函数说明：初始化i2c总线。

	scl：SCL信号线的I/O口
	sda：SDA信号线的I/O口
	freq：SCL时钟频率
示例：

```py
i2c.init(scl=Pin(22),sda=Pin(21))
```

#### 2. I2C.scan()
函数说明：扫描0x08到0x77之间的I2C地址，并返回设备列表。
<br>示例：

```py
i2c.scan()
```

#### 3. I2C.start()
函数说明：在总线上触发START状态（SCL为高电平时，SDA转为低电平）。
<br>示例：

```py
i2c.start()
```

#### 4. I2C.stop()
函数说明：在总线上触发STOP状态 (SCL为高电平时，SDA转为高电平)。
<br>示例：

```py
i2c.stop()
```

#### 5. I2C.write(buf)
函数说明：buf中的数据写入到总线，并返回写入的字节数。

	buf：存储数据的缓冲区
**注意**：
<br>&nbsp;&nbsp;&nbsp;使用write()函数时要与start函数一起使用，否则无法返回写入的字节数。
<br>示例：

```py
buf = b'123'
i2c.start()
i2c.write(buf)
```

#### 6. I2C.readinto(buf, nack=True)
函数说明：从总线上读取数据并存放到buf，无返回值。

	buf：存储数据的缓冲区
**注意：**
<br>&nbsp;&nbsp;&nbsp;读取的字节数是buf的长度。在接收到最后一个字节之前，总线将发送ACK信号。在接收到最后一个字节后，如果nack为True，那么将发送一个NACK信号，否则将发送一个ACK信号。
示例：

```py
buf=bytearray(3)
i2c.readinto(buf)
```

### 其他类函数

####  **标准总线操作**
&nbsp;&nbsp;&nbsp;下面函数是标准的I2C主模式读写操作。

#### 1. I2C.readfrom(addr, nbytes)
函数说明：从指定地址设备读取数据，返回读取对象，这个对象与I2C设备有关。

	addr：i2c设备地址（可由scan函数读取出来）
    nbytes：要读取数据的大小
示例：

```py
>>> print(i2c.scan())
[24]
>>> data = i2c.readfrom(24, 8)
>>> print(data)
b'\x00\x02\x00\x00\xe8\x03\xe8\x03'
```

#### 2. I2C.readfrom_into(addr, buf)
函数说明：从指定地址设备读取buf.len()个数据到buf。

	addr：i2c设备地址（可由scan函数读取出来）
	buf：存储数据的缓冲区
示例：

```py
>>> buf = bytearray(8)
>>> i2c.readfrom_into(24, buf)
>>> print(buf)
bytearray(b'\x00\x02\x00\x00\xe8\x03\xe8\x03')
```

#### 3. I2C.writeto(addr, buf)
函数说明：将buf中的数据写入设备，并返回写入数据的大小。

	addr：i2c设备地址（可由scan函数读取出来）
	buf：存储数据的缓冲区
示例：

```py
>>> b = bytearray(3)
>>> b[0] = 24
>>> b[1] = 111
>>> b[2] = 107
>>> i = i2c.writeto(24,b)
3
```
#### **内存操作**
&nbsp;&nbsp;&nbsp;某些 I2C 设备作为存储设备 (或一组寄存器) ，可以读取或者写入。这种情况下，有两个地址和 I2C 事务相关: 从设备地址和内存地址。下面方法用于和这些设备进行通信。
#### 1. I2C.readfrom_mem(addr, memaddr, nbytes, addrsize=8)
函数说明：从I2C设备的寄存器中读取并返回数据。

	addr：i2c设备地址（可由scan函数读取出来）
	memaddr：寄存器地址
    nbytes：要读取的字节大小
    addrsize：指定地址大小，默认为8位（在ESP8266上这个参数无效，地址大小总是8位）
示例：

```py
b = i2c. readfrom_mem(24,  0x58, 3)
print(b)
```
运行结果：

```py
b'\x00\x02\x01'
```
#### 2. I2C.readfrom_mem_into(addr, memaddr, buf, addrsize=8)
函数说明：从I2C设备的寄存器中读取buf.len()个数据到buf，无返回值。

	addr：i2c设备地址（可由scan函数读取出来）
	memaddr：寄存器地址
    buf：存储数据的缓冲区
    addrsize：指定地址大小，默认为8位（在ESP8266上这个参数无效，地址大小总是8位），读取数据数量是buf的长度。
示例：

```py
buf=bytearray(8)
i2c.readfrom_mem_into(24, 0x58, buf)
```
#### 3. I2C.writeto_mem(addr, memaddr, buf, addrsize=8)
函数说明： 将buf 中的数据全部写入到从设备 addr 的内存 memaddr。
		
    addr：i2c设备地址（可由scan函数读取出来）
	memaddr：寄存器地址
	buf：存储数据的缓冲区
    addrsize：指定地址大小，默认为8位（在ESP8266上这个参数无效，地址大小总是8位），读取数据数量是buf的长度。
示例：

```py
buf = b'123'
i2c.writeto_mem(24, 0x58, buf)
```
### 综合示例

```py
from machine import I2C, Pin

i2c0 = I2C(scl=Pin(22), sda=Pin(21), freq=100000)

addr0 = i2c0.scan() #扫描I2C地址，以列表的形式返回设备地址
addr0 = addr0[0] #列表转换成int
print("address:", hex(addr0)) #打印I2C设备地址

buf1 = bytearray(5)
buf1 = i2c0.readfrom(addr0, 5) #从I2C设备地址addr0读取5个字节
print("readfrom():", buf1) #打印缓冲区内容
```
运行结果：

```py
address: 0x18
readfrom(): b'\x00\x02\x00\x00\xe8'
```