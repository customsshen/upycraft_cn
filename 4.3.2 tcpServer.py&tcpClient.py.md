## 基于TCP协议的通信

### 准备
硬件：
* FireBeetle-ESP32 × 1

软件：
* uPyCraft IDE
* 串口调试助手  <p>下载地址：git.oschina.net/dfrobot/upycraft/raw/master/sscom5.12.1.exe

代码位置：
* <p>服务器端：File → Examples → Communicate → <b>tcpServer.py </b></p>
* <p>客户端：File → Examples → Communicate → <b>tcpClient.py</b></p>

### 实验步骤

### **TCPServer**

<p>1. 修改tcpServer.py 文件中的WiFi名称和密码，并下载运行，如下图</p>

<img src=images/5.22.png>

<p>2. 打开串口调试工具，端口号处选择TCPClient，将SSCOM中远程地址修改为IDE终端打印的tcp服务器IP地址，端口号与tcpServer.py中端口号相同，然后点击连接，如下图</p>

<img src=images/5.23.png>

<p>3. 连接后可在SSCOM输入框中输入你想要发送的内容，点击发送，服务器即可收到客户端发送的信息。</p>

#### 实验效果
<img src=images/5.24.png >

### **TCPClient**
<p>如下图</p>

<img src=images/5.25.png>

<p>1. 打开SSCOM，在端口号处选择TCPServer，将SSCOM本地IP地址修改为服务器的IP地址，并选择合适的端口，并点击侦听，如下图</p>

<img src=images/5.26.png>

<p>2. 修改 tcpClient.py 文件中的WiFi名称和密码，并按照SSCOM修改host和port，修改完后下载运行，如下图</p>

<img src=images/5.25.0.png>

<p>3. 下载运行后，在SSCOM可以看到客户端发送的信息——“hello DFRobot,I am TCP Client”，在SSCOM输入框中输入字符串，点击发送，客户端即可收到该信息</p>

### 实验效果
<img src=images/5.27.png>
