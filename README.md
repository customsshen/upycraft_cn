### 下载

- [ ] <a href="https://raw.githubusercontent.com/DFRobot/uPyCraft/master/uPyCraft.exe" title="uPyCraft" target="_blank">点击下载</a> uPyCraft IDE （Windows）

- [ ] <a href="https://git.oschina.net/dfrobot/upycraft/raw/master/uPyCraft_linux_V0.30" title="uPyCraft" target="_blank">点击下载</a> uPyCraft IDE （Linux）



<img src=images/85.png>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Python已经逐渐成为热门编程语言的“座上宾”，同时它也是一款比较容易上手的脚本语言，而且有强大的社区支持，一些非计算机专业领域的人都选它作为入门语言。因此也就出现了将Python应用到嵌入式领域的MicroPython，MicroPython脱胎于Python，基于ANSI C（C语言标准），然后在语法上又遵循了Python的规范，主要是为了能在嵌入式硬件上（这里特指微控制器级别）更易于实现对底层的操作。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uPyCraft是一个可运行在Windows平台的MicroPython编程IDE，其界面简洁，操作便利，适合新手的学习和使用。uPyCraft IDE内置了许多基础操作库，为众多的MicroPython爱好者提供了一个简单实用的平台。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;本文档在编写的过程中，强调实用，易用和有用。文档分为5个章节，第1章节主要介绍MicroPython语言的背景和特点，以及MicroPython的基本语法；第2章介绍uPyCraft软件的使用；第3章讲基于ESP32的MicroPython模块，各个功能模块的特点和库函数；第4章用具体的实例详细介绍如何使用这些库函数实现功能模块的不同应用；第5章是配套的学习资源（包括硬件资源和软件资源），后面有更新的学习资源和例子都会发出链接给大家。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;第1章是MicroPython语法基础，如果是新手建议先看第2章，知道如何使用uPyCraft IDE后，再看第1章。在学习MicroPython基本语法的过程中动手在uPyCraft上实践操作，能大大提高你的学习效率。如果你已经掌握了MicroPython语法请忽略第1章。在第3章中，我们介绍了常用的MicroPython模块，通过使用这些模块，无须深入掌握细节，你也可以轻松应用每一个外设。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;使用uPyCraft IDE结合本文档学习MicroPython，需要你有基本的语言基础，不需要有硬件方面的知识，通过本文档的学习，你可以很快上手做些小项目。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;uPyCraft IDE上有各个实例的源代码，这些源代码都在开发板上验证通过。希望广大读者不要只是把源码一烧了之，而是应该尝试自己编写这些代码，因为只有经过不断的实践，才能获得真知。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;由于涉及的知识日新月异，难免有差错和不足之处，希望广大读者批评指正。有任何意见或疑问，可以和我们联系。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;我们的QQ群为：[619558168](http://qm.qq.com/cgi-bin/qm/qr?k=m1EY0mIdfE4x6BcFB6wGCj0iJm3tjBuP)，当然你也可以[访问论坛](http://www.dfrobot.com.cn/community/forum-157-1.html)进行交流。