#### 1. dir(obj)
函数说明：查看对象中所有属性和函数。
<br>示例：

```py
>>> import sys
>>> dir(sys)
['__name__', 'path', 'argv', 'version', 'version_info', 'implementation', 'platform', 'byteorder', 'maxsize', 'exit', 'stdin', 'stdout', 'stderr', 'modules', 'print_exception']
>>> dir(sys.argv)
['append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse', 'sort']
```

#### 2. encode()
函数说明：将字符串转化为字符编码，并返回一个字节数组。
<br>示例：

```py
>>> str = "DFRobot"
>>> strEn = str.encode()
>>> print(strEn)
b'DFRobot'
>>>
>>> print(str[1])
F
>>> print(strEn[1])
70
```

#### 3. find(str[, beg=0, end=len(s)-1])
函数说明：从左向右查找字符串中是否有str子字符串，如果有则返回第一次出现子字符串的位置下标，否则返回-1。
<br>&nbsp;&nbsp;&nbsp;如果没有指定第二个和第三个参数，则默认是从字符串下标0开始查找，直到字符串结束。
<br>示例：

```py
>>> str = "DFRobot"
>>> print( str.find("bo") )
4
>>> print(str.find("Bo"))
-1
```
&nbsp;&nbsp;&nbsp;如果指定了第二个参数beg，则会从字符串下标beg的位置开始查找，如果指定了三个参数end，则会在字符串下标end的位置结束查找。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.find("o", 1))
3
>>> print(str.find("o", 3, 6))
3
```

#### 4. rfind(str[, beg=0, end=len(s)-1])
函数说明：从左向右查找字符串中是否有str子字符串，如果有则返回最后一次出现子字符串的位置下标，否则返回-1。
<br>&nbsp;&nbsp;&nbsp;如果没有指定第二个和第三个参数，则默认是从字符串下标0开始查找，直到字符串结束。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.rfind("o"))
5
>>> print(str.rfind("B"))
-1
```
&nbsp;&nbsp;&nbsp;如果指定了第二个参数beg，则会从字符串下标beg开始查找，如果指定了三个参数end，则会在字符串下标end结束查找。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.find("o", 1))
3
>>> print(str.rfind("o", 4, 6))
5
```

#### 5. index(str[, beg=0, len=len(s)-1])
函数说明：从左向右查找字符串中是否有str子字符串，如果有则返回第一次出现子字符串的位置下标，如果没有找到，系统会抛出异常”ValueError: substring not found”提示没有找到这个子字符串，并且终断程序执行。
<br>&nbsp;&nbsp;&nbsp;如果没有指定第二个和第三个参数，则默认从字符串下标0开始查找，如果直到字符串末尾还没有找到，系统就会抛出异常。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.index("o"))
3
>>> print(str.index("B"))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: substring not found
```
&nbsp;&nbsp;&nbsp;如果指定了第二个参数beg，则会从字符串下标beg开始查找，如果指定了三个参数end，则会在字符串下标end结束查找。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.index("o", 2))
3
>>> print(str.index("o", 4, 3))
5
```

#### 6. rindex(str[, beg=0, len=len(s)-1])
函数说明：从左向右查找字符串中是否有str子字符串，如果找到则返回最后一次出现子字符串的位置下标，如果没有找到，系统会抛出异常”ValueError: substring not found”提示没有找到这个子字符串，并且终断程序执行。
<br>&nbsp;&nbsp;&nbsp;如果没有指定第二个和第三个参数，则默认从字符串下标0开始查找，如果直到字符串末尾还没有找到，系统就会抛出异常。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.rindex("o"))
5
>>> print(str.rindex("B"))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: substring not found
```
&nbsp;&nbsp;&nbsp;如果指定了第二个参数beg，则会从字符串下标beg开始查找，如果指定了三个参数end，则会在字符串下标end结束查找。
<br>示例：

```py
>>> str = "DFRobot"
>>> print(str.rindex('o', 2))
5
>>> print(str.rindex('o', 0, 4))
3
```

#### 7. str.join(a)
函数说明：将字符串、元组、列表或字典中的元素以指定的字符（或字符串）连接。

	str：分隔符
    a：要连接的列表、元组、字典、字符串等。
示例：

```py
>>> a = ["hello", "world", "hello", "DFRobot"]
>>> str = ':'
>>> print(str.join(a))
'hello:world:hello:DFRobot'
>>>
>>> a = "hello world"
>>> print(' '.join(a))
'h e l l o   w o r l d'
```

#### 8. str.split(a[, num])
函数说明： 拆分字符串。通过指定分隔符对字符串进行切片，以列表的形式返回被分割的字符串碎片。

	str：分隔符
    num：分割次数，
    	注意：如果存在参数num，则仅分隔成 num+1 个子字符串
    
**注意：**
<br>&nbsp;&nbsp;&nbsp;当使用空格作为分隔符时，对于中间为空的项会自动忽略。
<br>示例：

```py
>>> str = "www.dfrobot.com.cn"
>>> print(str.split('.'))
['www', 'dfrobot', 'com', 'cn']
>>> 
>>> print(str.split('.', 1))
['www', 'dfrobot.com.cn']
>>> 
>>> print(str.split('.', 3))
['www', 'dfrobot', 'com', 'cn']
>>> 
>>> print(str.split('.', 3)[2])
com
```

#### 9. splitlines([keepends=False])
函数说明：以换行符（\r）或回车符（\n）为标志分割对象字符串，以列表的形式返回被分割的子字符串。

	keepends：是否保留分隔符号
    	True、False
示例：

```py
>>> str = "www\rdfrobot\ncom\rnrcn\r\rhello"
>>> print(str.splitlines())  #不保留分隔符
['www', 'dfrobot', 'com', 'nrcn', '', 'hello']
>>> 
>>> print(str.splitlines(True))  #保留分隔符
['www\r', 'dfrobot\n', 'com\r', 'nrcn\r', '\r', 'hello']
```

#### 10. rsplit(str, num)
函数说明：拆分字符串。通过指定分隔符对字符串进行切片（从右到左），以列表的形式返回被分割的子字符串。

**注意：**
<br>&nbsp;&nbsp;&nbsp;不会返回分隔符

	str：分割标志
    num：分割次数
    
<br>示例：

```py
>>> str = "www.DFRobot.com.cn"
>>> print(str.rsplit('.'))
['www', 'DFRobot', 'com', 'cn']
>>> 
>>> print(str.rsplit('.', 1))
['www.DFRobot.com', 'cn']
>>> 
>>> print(str.rsplit('.', 3))
['www', 'DFRobot', 'com', 'cn']
```

#### 11. startswith(str[, beg])
函数说明：用于检查字符串是否以指定字符串开头，如果是则返回 True，否则返回 False。

	str：待检查的字符串
	beg：开始检查的位置（字符串下标）。
示例：

```py
>>> str = "www.dfrobot.com.cn"
>>> print(str.startswith('df'))
False
>>> print(str.startswith('df', 1) )
False
>>> print(str.startswith('df', 4))
True
```

#### 12. endswith(str)
函数说明：用于检查字符串是否以指定字符串结尾，如果是返回True，否则返回False。
<br>示例：

```py
>>> str = "www.dfrobot.com.cn"
>>> print(str.endswith('com') )
False
>>> print(str.endswith('cn'))
True
```

#### 13. strip([str])
函数说明：去掉字符串头尾指定的字符串（默认为空格）。成功返回去掉指定字符的字符串，否则返回原字符串。
<br>示例：

```py
>>> str = "www.dfrobot.com.cn"
>>> print(str.strip()) #去掉空格
www.dfrobot.com.cn
>>> print(str.strip('www') )
.dfrobot.com.cn
>>> print(str.strip('.cn'))
www.dfrobot.com
>>> print(str.strip('com')) #失败，返回原字符串
www.dfrobot.com.cn
```

#### 14. lstrip([str])
函数说明：去掉字符串左边指定字符串（默认为空格），返回新的字符串，如果没有找到所指定的字符串，则返回原字符串。
<br>示例：

```py
>>> str = "    www.dfrobot.com.cn"
>>> print(str.lstrip())  #去掉空格
www.dfrobot.com.cn
>>> str = "www.dfrobot.com.cn"
>>> print(str.lstrip('w'))
.dfrobot.com.cn
```

#### 15. rstrip([str])
函数说明：去掉字符串右边指定字符串（默认为空格），返回新的字符串，如果没有找到所指定的字符串，则返回原字符串。
<br>示例：

```py
>>> str = "www.dfrobot.com.cn  "  #去掉空格
>>> print(str.rstrip())
www.dfrobot.com.cn
>>> 
>>> str = "www.dfrobot.com.cn"
>>> print(str.rstrip('.com.cn'))
www.dfrobot
```

#### 16. format(\*args)
函数说明：字符串格式化函数。
<br>&nbsp;&nbsp;&nbsp;format()函数可以接受不限个参数，位置可以不按顺序。
<br>示例：

```py
>>> print( "{}{}".format("hello", "world") )
helloworld
>>> print( "{1} {0} {1}".format("hello", "world") )
world hello world
>>> print( "1+2={0}, 1*2={1}".format(3, 2) )
1+2=3, 1*2=2
```
&nbsp;&nbsp;&nbsp;format()函数也可以设置参数。
<br>示例：

```py
>>> str = "web_name:{name}, address:{url}"
>>> site = {"name":"DFRobot", "url":"www.dfrobot.com.cn"}
>>> print(str.format(**site))
web_name:DFRobot, address:www.dfrobot.com.cn
>>> 
>>> print(str.format(name="DFRobot", url="www.dfrobot.com.cn"))
web_name:DFRobot, address:www.dfrobot.com.cn
```
&nbsp;&nbsp;&nbsp;数字格式化，更多的用法这里不做介绍。
<br>示例：保留小数点后两位

```py
>>> print("{:.2f}".format(3.1415926))
3.14
```

#### 17. replace(old, new[, num])
函数说明：字符串替换。

	old：被替换的字符串
    new：替换的字符串
    num：替换的次数
示例：

```py
>>> str = "aabbaa"
>>> print(str.replace("aa", "ee"))   #将aa替换成ee
eebbee
>>> print(str.replace("aa", "ee", 1))  #将第一次出现的aa替换成ee
eebbaa
```

#### 18. count(str[, beg, end])
函数说明：统计字符串里某个字符串出现的次数。

	beg（end）：开始（结束）统计的字符串位置。
示例：

```py
>>> str = "DFRobot"
>>> print(str.count('o'))
2
>>> print(str.count('o', 4))
1
>>> print(str.count('o', 1, 2))
0
```

#### 19. partition(str)
函数说明：根据指定的分隔符将字符串进行分割（从左往右），并返回第一次出现的位置。

**注意：**
<br>&nbsp;&nbsp;&nbsp;如果字符串包含指定的分隔符，则返回一个3元的元组，第一个为分隔符左边的子串，第二个为分隔符本身，第三个为分隔符右边的子串。
示例：

```py
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.partition('!'))
('Be brave', '!', ' Break things! Learn and have fun!')
```

#### 20. rpartition(str)
函数说明：与partition()函数类似，只是分隔方向是从右往左。
<br>示例：

```
>>> str = "DFRobot"
>>> print(str.rpartition('o'))
('DFRob', 'o', 't')
```

#### 21. center(num)
函数说明：在字符串两边填充多个空格。

	num：填充空格的个数
示例：

```py
>>> str = "hello"
>>> print(str.center(14), 'flag')
    hello      flag
```

#### 22. lower()
函数说明： 将字符串中所有大写字母转换为小写。
<br>示例：

```py
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.lower())
'be brave! break things! learn and have fun!'
```

#### 23. upper()
函数说明： 将原字符串中所有小写字母转换为大写。
<br>示例：

```py
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.upper())
'BE BRAVE! BREAK THINGS! LEARN AND HAVE FUN!'
```

#### 24. isspace()
函数说明： 检测字符串是否只由空格组成，如果字符串中只包含空格，则返回 True，否则返回 False。
<br>示例：

```py
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.isspace())
False
>>> str = "            "
>>> print(str.isspace())
True
```

#### 25. isalpha()
函数说明： 检测字符串是否只由字母组成，如果是则返回True，否则返回False。
<br>示例：

```py
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.isalpha())
False
>>> str = "BebraveBreakthingsLearnandhavefun"
>>> print(str.isalpha())
True
```

#### 26. isdigit()
函数说明： 检测字符串是否只由数字组成，如果是则返回True，否则返回False。
<br>示例：

```py
>>> str = "123456"
>>> print(str.isdigit())
True
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.isdigit())
False
```

#### 27. isupper()
函数说明： 检测字符串中所有的字母是否都为大写，如果是则返回True，否则返回False。
<br>示例：

```py
>>> str = "BE BRAVE! BREAK THINGS! LEARN AND HAVE FUN!"
>>> print(str.isupper())
True
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.isupper())
False
```

#### 28. islower()
函数说明： 检测字符串中所有的字母是否都为小写，如果是则返回True，否则返回False。
<br>示例：

```py
>>> str = "Be brave! Break things! Learn and have fun!"
>>> print(str.islower())
False
>>> str = "be brave! break things! learn and have fun!"
>>> print(str.islower())
True
```