&nbsp;&nbsp;&nbsp;time库用来获取时间和日期、测量时间间隔、延时时间等。

### 函数

#### 1. time.sleep(sec)
函数说明：睡眠给定的秒数。
	
	sec：睡眠时间
		可以为浮点数，整数
示例：

```py
>>> time.sleep(3)
>>> def mySleep():
...   time.sleep(3)
...   print("hello")
... 
>>> mySleep()
hello
```

&nbsp;&nbsp;&nbsp;执行mySleep()函数后，你会发现过了三秒才会打印“hello”。
<br>&nbsp;&nbsp;&nbsp;其他类似time(sec)的函数：
<br>&nbsp;&nbsp;&nbsp;time.sleep_ms(ms)：睡眠给定的毫秒数，为int型。
<br>&nbsp;&nbsp;&nbsp;time.sleep_us(us)：睡眠给定的微秒数，为int型。

#### 2. time.time()
函数说明：获取当前cpu时间戳，单位：秒。
<br>示例：

```py
>>> import time
>>> print(time.time())
21690
```

#### 3. time.ticks_ms()
函数说明：返回不断递增的毫秒计数器，在某些值后会重新计数。计数毫无意义，除非在ticks.diff()中。
<br>示例：

```py
>>> print(time.ticks_ms())
24612257
```
&nbsp;&nbsp;&nbsp;其他类似ticks_ms()的函数：
<br>&nbsp;&nbsp;&nbsp;time.ticks_us()：返回微秒。
<br>&nbsp;&nbsp;&nbsp;time.ticks_cpu()：相比前面的函数具有更高精度（返回CPU时钟）。

#### 4. time.ticks_add(ticks, delta)
函数说明：经过偏移时间后的时间戳。

	ticks：
    	ticks_ms()、ticks_us()、ticks_cpu()
    delta：任意整数或表达式（注意：必须使用tick_diff功能来处理截止日期）
示例：

```py
>>> def ticks_addTest():
...   print(time.ticks_ms())
...   time.sleep(2)
...   print(time.ticks_add(time.ticks_ms(), -2))
... 
>>> ticks_addTest()
23064376
23066374
```

#### 5. time.ticks_diff(old_t, new_t)
函数说明：计算两次调用 ticks_ms(), ticks_us(), 或 ticks_cpu()之间的时间。

	old_t：开始时间。
    new_t：结束时间。
示例：

```py
>>> def ticks_diffTest():
...   t1 = time.ticks_ms()
...   time.sleep(2)
...   t2 = time.ticks_ms()
...   t = time.ticks_diff(t1, t2)
...   print(t)
... 
>>> ticks_diffTest()
-2000
```