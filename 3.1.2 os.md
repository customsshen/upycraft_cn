&nbsp;&nbsp;&nbsp;该模块包含文件系统访问和urandom的功能。

### 函数

#### 1. os.uname()
函数说明：查看系统信息。
<br>示例：

```py
>>> import os
>>> os.uname()
(sysname='esp32', nodename='esp32', release='1.9.1', version='v1.9.1-224-g83d3f3f-dirty on 2017-12-12', machine='ESP32 module with ESP32')
```

#### 2. os.listdir(path)
函数说明：没有参数时显示当前目录，给定参数时显示给定的目录。
<br>示例：

```py
>>> os.listdir()
['boot.py', 'lib']
>>> os.listdir("./lib")
['test.py']
```

#### 3. os.mkdir(path)
函数说明：创建目录，path为创建目录的路径。
<br>示例：

```
>>> os.listdir()
['boot.py']
>>> path = "./lib"
>>> os.mkdir(path)
>>> os.listdir()
['boot.py', 'lib']
```

#### 4. os.rmdir(path)
函数说明：删除目录。
<br>示例：

```
>>> os.listdir()
['boot.py', 'lib']
>>> os.rmdir("./lib")
>>> os.listdir()
['boot.py']
```

#### 5. os.remove(path)
函数说明：删除文件。
<br>示例：

```py
>>> os.listdir("./lib")
['test.py']
>>> os.remove("./lib/test.py")
>>> os.listdir("./lib")
[]
```



#### 6. os.getcwd()
函数说明：查看当前目录。
<br>示例：

```py
>>> os.getcwd()
'/'
```

#### 7. os.chdir(path)
函数说明：更改当前目录。
<br>示例：

```py
>>> os.getcwd()
'/'
>>> os.chdir("./lib")
>>> os.getcwd()
'/lib'
>>> os.chdir("..")
>>> os.getcwd()
'/'
```

#### 8. os.rename(old_path, new_path)
函数说明：重命名文件。
<br>示例：

```py
>>> os.listdir(os.getcwd())
['test.py']
>>> os.rename("test.py", "mytest.py")
>>> os.listdir(os.getcwd())
['mytest.py']
```

#### 9 . os.stat(path)
函数说明：获取文件或目录的状态。
<br>示例：

```py
>>> os.stat("./lib")
(16384, 0, 0, 0, 0, 0, 0, 0, 0, 0)
>>> os.stat("./lib/test.py")
(32768, 0, 0, 0, 0, 0, 1, 0, 0, 0)
```

#### 10. os.urandom(n)
函数说明：用n个随机字节返回一个字节对象。
<br>示例：

```py
>>> os.urandom(20)
b'f\x92\x85t28\xa1\xf0\xaf3\xf5\xd9\xcdx\xc3\n\xedm\xf8\xb7'
```

