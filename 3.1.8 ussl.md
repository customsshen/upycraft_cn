&nbsp;&nbsp;&nbsp;此模块提供了对网络套接字（客户端和服务器端）的传输安全性（以前称为“安全套接字层”）加密和对等身份验证功能。
### 函数

#### ussl.wrap_socket(sock，server_side = False，keyfile = None，certfile = None，cert_reqs = CERT_NONE，ca_certs = None)
函数说明：ssl是专门用来处理https的模块，我们使用该模块的wrap_socket函数生成一个SSLSocket对象，然后建立连接。