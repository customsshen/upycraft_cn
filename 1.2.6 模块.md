&nbsp;&nbsp;&nbsp;前面学习了如何使用函数，通过函数能够在程序中实现代码的重用，那么当需要在程序中重用其他程序内的代码，应该怎么办？
<br>&nbsp;&nbsp;&nbsp;我们可以通过模块来调用，通过在程序中引用模块，就可以使用其中的函数和变量，这也是使用Python标准库的方法。
<br>&nbsp;&nbsp;&nbsp;在模块中，模块的名称（作为字符串）可用作全局变量值__name__（有关内容查看第3章）。

### 1.2.6.1 import语句
&nbsp;&nbsp;&nbsp;MicroPython中要引入模块，使用import语句，格式如下：

```
import <模块名>
```
**注意：**
<br>&nbsp;&nbsp;&nbsp;如果是直接引入模块，在使用模块中函数或属性（常量、变量）时一定要指出函数或属性的所属模块，格式为：<模块名>.<函数或属性>，否则会出错。
<br>示例：

```py
>>> import random
>>> 
>>> num = random.randint(1, 100) #使用random模块中的randint()函数
>>> print(num)
7
```

### 1.2.6.2 from…import语句
&nbsp;&nbsp;&nbsp;如果只想引入模块中的某个函数或属性，使用from…import语句，格式如下：

		from <模块名> import <函数名或变量名>
示例：

```py
>>> from random import randint
>>> 
>>> num = randint(1, 100)
>>> print(num)
95
```
&nbsp;&nbsp;&nbsp;在使用from...import语句从模块中引入函数时，为避免冲突和便于理解，可以使用as语句给引入的函数换个名字，如下

		from <模块名> import <函数名或变量名> as <自定义名>
示例：

```py
>>> from random import randint as ra
>>> 
>>> num = ra(1, 100)
>>> print(num)
30
```

### 1.2.6.3 自定义的模块
&nbsp;&nbsp;&nbsp;每个Python文件，只要它保存在MicroPython的文件系统中，就是一个模块。
<br>&nbsp;&nbsp;&nbsp;引入自定义的模块，需要模块文件位于MicroPython环境变量路径下或与当前运行程序在同一路径下。

**注意：**
<p id="color" style="color:purple;">&nbsp;&nbsp;&nbsp;不能引入workSpace目录中的文件，编写好后，必须要下载到板子上才可以引入它.</p>

示例：
<p>1) 自定义模块，命名为 DefMod.py</p>

<img src=images/1.2.6图1.png>

<p>2) RefMod.py 中引入自定义模块</p>

<img src=images/3.1.2_9.png>

### 1.2.6.4 dir()函数
&nbsp;&nbsp;&nbsp;dir()函数是micropython内置的函数，用来列出模块中的函数、类和属性。
<br>&nbsp;&nbsp;&nbsp;如果给dir()函数提供一个模块名称，它返回该模块中的名称列表，如果不提供，则返回当前模块的名称列表。
<br>示例：

```py
>>> import random
>>> 
>>> dir(random) 
['__name__', 'getrandbits', 'seed', 'randrange', 'randint', 'choice', 'random', 'uniform']
>>> print( dir() )
['myfile', 'print_x', 'a', 'gc', 'print_abc', 'num', 'sys', 'bdev', 'randint', '__name__', 'os', 'x', 'random', 'print_max', 'ra', 'uos']
```
&nbsp;&nbsp;&nbsp;dir()函数没有列出内置函数和变量的名字，这些函数和变量定义在builtins模块中，通过它你可以看到具体的内容。
<br>示例：

```py
>>> import builtins
>>> dir(builtins)
['__name__', '__build_class__', '__import__', '__repl_print__', 'bool', 'bytes', 'bytearray', 'complex', 'dict', 'enumerate', 'filter', 'float', 'frozenset', 'int', 'list', 'map', 'memoryview', 'object', 'property', 'range', 'reversed', 'set', 'slice', 'str', 'super', 'tuple', 'type', 'zip', 'classmethod', 'staticmethod', 'Ellipsis', 'NotImplemented', 'abs', 'all', 'any', 'bin', 'callable', 'compile', 'chr', 'delattr', 'dir', 'divmod', 'eval', 'exec', 'execfile', 'getattr', 'setattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'isinstance', 'issubclass', 'iter', 'len', 'locals', 'max', 'min', 'next', 'oct', 'ord', 'pow', 'print', 'repr', 'round', 'sorted', 'sum', 'BaseException', 'ArithmeticError', 'AssertionError', 'AttributeError', 'EOFError', 'Exception', 'GeneratorExit', 'ImportError', 'IndentationError', 'IndexError', 'KeyboardInterrupt', 'KeyError', 'LookupError', 'MemoryError', 'NameError', 'NotImplementedError', 'OSError', 'OverflowError', 'RuntimeError', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SystemExit', 'TypeError', 'UnicodeError', 'ValueError', 'ZeroDivisionError', 'input', 'open']
```